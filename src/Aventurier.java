public class Aventurier {

    private Coordonnee initCoordonnee;

    public Aventurier(Coordonnee initCoordonnee) {
        this.initCoordonnee = initCoordonnee;
    }

    public Coordonnee deplace(char item) {
        int x = this.initCoordonnee.getX() ;
        int y = this.initCoordonnee.getY();
        switch (item){
            case 'N' : y = y-1;  break;
            case 'S' : y = y+1;  break;
            case 'E' : x = x+1;  break;
            case 'O' : x = x-1;  break;
        }
        Coordonnee c = new Coordonnee(x,y);
        this.initCoordonnee = c ;
        return c;
    }
}
