import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class JeuAventurier {

    public static void main(String[] args) {

        Path pathCarte = Paths.get("resources", "carte.txt");
        Path path = Paths.get("resources", "deplacement_1.txt");

        try {
            List<String> linesCarte = Files.readAllLines(pathCarte);

//            linesCarte.forEach(line -> {
//                // les positions vides
//                IntStream.range(0, line.length())
//                        .filter(i -> line.charAt(i) == ' ')
//                        .mapToObj(i -> new Coordonnee(i, linesCarte.indexOf(line)))
//                        .forEach(c -> System.out.println("(" + c.getX() + "," + c.getY() + ")"));
//            });

            List<Coordonnee> coordonneeListBois = new ArrayList<>();

            linesCarte.forEach(line -> {
                // les positions vides
                List<Coordonnee> lsc = IntStream.range(0, line.length())
                        .filter(i -> line.charAt(i) == '#')
                        .mapToObj(i -> new Coordonnee(i, linesCarte.indexOf(line)))
                        .peek(c -> System.out.println("(" + c.getX() + "," + c.getY() + ")"))
                        .collect(Collectors.toList());
                coordonneeListBois.addAll(lsc);
            });

            List<String> lines = Files.readAllLines(path);
            String lineCoordonnes = lines.get(0);
            String[] s = lineCoordonnes.split(",");
            Coordonnee coordonneeInit = new Coordonnee(Integer.parseInt(s[0]), Integer.parseInt(s[1]));
            System.out.println("position initiale : (" + coordonneeInit.getX() + " ," + coordonneeInit.getY() + ")");

            String lineDeplacement = lines.get(1);
            System.out.println("Suite de déplacement : " + lineDeplacement);

            Aventurier aventurier = new Aventurier(coordonneeInit);

            List<Coordonnee> lcv =lineDeplacement.chars()
                    .mapToObj(item -> aventurier.deplace((char) item))
                    .filter(c -> c.getX() > 0 && c.getX() < 20 && c.getY() > 0 && c.getY() < 20)
                    .peek(c -> System.out.println("(" + c.getX() + "," + c.getY() + ")"))
                    .filter( c -> !coordonneeListBois.contains(c))
                    .peek(c -> System.out.println("(" + c.getX() + "," + c.getY() + ")"))
                    .collect(Collectors.toList());

            lcv.forEach(System.out::println);


        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
